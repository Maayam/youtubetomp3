import sys
import pathlib
from pytube import YouTube
import re
import tkinter as tk
from tkinter import simpledialog, messagebox

def sanitize_filename(filename):
    # Replace characters not allowed in filenames with underscores
    sanitized_filename = re.sub(r'[^\w\-. ]', '_', filename)
    # Remove any leading or trailing spaces
    sanitized_filename = sanitized_filename.strip()
    # Remove consecutive underscores
    sanitized_filename = re.sub(r'_+', '_', sanitized_filename)
    # Ensure filename is not empty
    if not sanitized_filename:
        sanitized_filename = "unnamed_file"
    return sanitized_filename

def main():
    if len(sys.argv) != 2:
        # Create Tkinter root window
        root = tk.Tk()
        root.withdraw()  # Hide the root window
        # Prompt the user for a url string
        url = simpledialog.askstring("Message", "Please provide url to youtube video:")
    else:
        url = sys.argv[1]

    if url is not None:
        video = YouTube(url)
        stream = video.streams.filter(only_audio=True).first()
        # get user download folder path using python paths
        download_folder = f"{str(pathlib.Path.home())}/Downloads"

        isOK = messagebox.askokcancel("Confirmation", f"Tool will download \"{video.title}\" to your Downloads folder ({download_folder}). Continue?")

        if not isOK:
            messagebox.showinfo("Message", "Download ancelled. Exiting.")
            return

        # download the file to the user's download folder
        filename = f"{download_folder}/{sanitize_filename(video.title)}.mp3"
        stream.download(filename=filename)
        messagebox.showinfo("Message", "Successfully fetched video to your Downloads folder.")
    else:
        messagebox.showinfo("Message", "No video url provided. Exiting.")

if __name__ == "__main__":
    main()