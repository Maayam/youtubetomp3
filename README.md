# About

a simple utility based on `pytube` pip module to download audio of a video.
it will download it to your user downloads folder
tested on windows only.

[DOWNLOAD LATEST HERE](https://gitlab.com/Maayam/youtubetomp3/-/releases)

# usage

## run with .exe

just run .exe file and follow the steps.

## run with python

`pipenv run python main.py [URL]`

where [URL] is the URL to your youtube video.

# Dev

## Presquisites

- You need python installed on your system
- you also need pipenv - `pip install pipenv`
- then install dependencies with - `pipenv install`

## compile a new .exe version

`pipenv run pyinstaller --onefile Mitune_e_YoutubeToMp3.py`
